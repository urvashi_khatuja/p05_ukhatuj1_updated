//
//  ViewController.h
//  p05_ukhatuj1_update
//
//  Created by urvashi khatuja on 5/13/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController

{
    NSTimer * BirdMove;
    //added in part 2
    NSTimer * ColMove;
}



@property (weak, nonatomic) IBOutlet UIImageView *colTop;

@property (weak, nonatomic) IBOutlet UIImageView *top;


@property (weak, nonatomic) IBOutlet UIImageView *colBottom;


@property (weak, nonatomic) IBOutlet UIImageView *bird;

@property (weak, nonatomic) IBOutlet UIButton *startButton;


- (IBAction)startGame:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *highScore;




@property (weak, nonatomic) IBOutlet UIImageView *bot;


@end

